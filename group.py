import warnings
import pandas as pd
import numpy as np
import visa
import scopesetup as setup
from matplotlib import pyplot as plt

# The fundamental problem with this is the inability to tell apart a muon that decays 
# in the ID and whose electron drifts into the ID apart from the muon that decays in the ID and 
# whose electron drifts into the OD. They do basically the same thing pulse wise.Best I can do
# is compare the magnitudes and take a guess, but it's a totally arbitrary cutoff.

# This analysis is brittle and janky. Looking back, I would done the ingestion with python, then
# switch to SQL for analysis. Pandas is a great tool, but this is just abusive.

def parseline(x):
    '''return tuple of ints in the line, eg "10 12 14 15" -> (10,12,14,15)'''
    return tuple(int(i) for i in x.split())    

def lenline(r, level):
    '''Return true if at least 'level' non nan indices in the dataframe.'''
    innercount = 0
    for i in r[1:]: # need to ditch the CxTrace....
        if not np.isnan(i).all():
            innercount += 1
    if innercount >= level:
        return True
    else:
        return False

def od_decay(row, level):
    '''
    return true if decay happened in OD else false

    not implemented
    '''
    pass

def id_decay(row, level):
    '''
    Figure out the number of valid pulses above a magic number. If greater than level, return True. If not, False.
    False signifies a non-contained decay, True signifies a contained id decay.
    '''
    config = setup.load_config()

    #find all ID pulses.
    id_pulse_list = []
    for i in row[5:9]:
        for j in i:
            if not np.isnan(i).all(): #filter out nans
                id_pulse_list.append(j)

    #find all pairs of OD pulses.
    od_pulse_list = []
    for i in row[1:5]:
        for j in i:
            if not np.isnan(i).all(): #filter out nans
                od_pulse_list.append(j)

    od_pulse_list.sort()#inplace sort

    od_pulse_pairs = [] #get all valid (i.e, chronological) possible pulse pairs
    for i in od_pulse_list:
        for j in od_pulse_list:
            if i < j:
                od_pulse_pairs.append((i,j))

    od_pulse_pairs = [od_pulse_pairs[i] for i in range(len(od_pulse_pairs)) if i == 0 or od_pulse_pairs[i] != od_pulse_pairs[i-1]] #ditch the duplicate pairs.

    #figure out which id pulses could fit in which od_pulse_pairs.
    real_id_pulse_list = []
    for id_pulse in id_pulse_list:
        for od_range in od_pulse_pairs:
            if id_pulse in range(od_range[0], od_range[1]+1):
                real_id_pulse_list.append(id_pulse)

    real_id_pulse_set = set(real_id_pulse_list) #one id pulse might fit into multiple od windows.

    #get the charge associated with the tube. find index, add 8 to get charge.
    charge_list = []
    for pulse in real_id_pulse_set:
        temp_index = (list(row).index([pulse]) + 8)
        charge_list.append(row.iloc[temp_index])

    print(charge_list)
    count = 0
    for val in charge_list:
        if val < config['magic_charge_size']:
            count += 1

    if count > level:
        return True
    
    else:
        return False

    
if __name__ == "__main__":
    config = setup.load_config()
    pd.set_option('display.max_rows', 10)
    warnings.simplefilter(action='ignore', category=FutureWarning) #kill the re deprecation warning

## Packing data into dataframes 
    df1 = pd.read_csv('processed_traces/C1.csv', delimiter=':', header=None)
    df2 = pd.read_csv('processed_traces/C2.csv', delimiter=':', header=None)
    df3 = pd.read_csv('processed_traces/C3.csv', delimiter=':', header=None)
    df4 = pd.read_csv('processed_traces/C4.csv', delimiter=':', header=None)
    df5 = pd.read_csv('processed_traces/C5.csv', delimiter=':', header=None)
    df6 = pd.read_csv('processed_traces/C6.csv', delimiter=':', header=None)
    df7 = pd.read_csv('processed_traces/C7.csv', delimiter=':', header=None)
    df8 = pd.read_csv('processed_traces/C8.csv', delimiter=':', header=None)

    dfs = [df1, df2, df3, df4, df5, df6, df7, df8]

    for df in dfs:
        df[1] = df[1].str.replace(r"\[","") #removing some brackets
        df[1] = df[1].str.replace(r"\]","")
        df[2] = df[2].str.replace(r"\[","")
        df[2] = df[2].str.replace(r"\]","")

        df.replace(r'^\s*$', np.nan, regex=True, inplace=True) #replace whitespace with NaN, this will be deprecated eventually.
        df.columns = ["Name", "Tube", 'Charge']


    tubelist = ["tube1", "tube2", "tube3", "tube4","tube5", "tube6", "tube7", "tube8"]
    id_tubelist = tubelist[4:]
    od_tubelist = tubelist[:4]
    
    tubes = pd.concat([df1["Name"],df1["Tube"], df2["Tube"], df3["Tube"],df4["Tube"], df5["Tube"], df6["Tube"], df7["Tube"], df8["Tube"]], axis = 1)

    tubes.columns = ["Name"] + tubelist

    tubes[tubelist] = tubes[tubelist].applymap(parseline, na_action = 'ignore') #convert string elements to list of integers

    charges = pd.concat([df1["Charge"], df2["Charge"], df3["Charge"],df4["Charge"], df5["Charge"], df6["Charge"], df7["Charge"], df8["Charge"]], axis = 1)
    charges.columns = ["charge_1", "charge_2", "charge_3", "charge_4", "charge_5", "charge_6", "charge_7", "charge_8"]
    val = pd.concat([tubes, charges], axis = 1)

    # Need to begin applying id_decay and od_decay functions on val dataframe. 
    # Should be something like val.apply(id_decay, args=(3,), axis = 1)
