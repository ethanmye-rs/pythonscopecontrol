import numpy as np
import pandas as pd
from scipy.integrate import simps
from scopesetup import load_config
import matplotlib.pyplot as plt
from scipy.signal import butter, medfilt, find_peaks, filtfilt, peak_widths
import json
import logging
import time
import inspect

def timer(func):
    '''Decorator function to time exectution of functions. test functions with @timer in class defintion.'''
    def f(*args, **kwargs):
        before = time.perf_counter()
        rv = func(*args, **kwargs)
        after = time.perf_counter()
        print(f"Time Elapsed for {func.__name__} is {after - before} seconds")
        return rv
    return f

class Trace: 
    def __init__(self, config, channel, traceno):
       self.channel = channel
       self.traceno = traceno
       self.config = config

    def __repr__(self):
        return (f"C{self.channel}Trace{self.traceno:05d}")

    def __len__(self):
        return len(self.peaks)

    def loadTrace(self):
        '''Load a trace from txt record into numpy array. About 10x slower than loadTracePD'''
        self.trace = np.loadtxt(self.config['data_folder'] +'/' + self.config['analyze_date'] + '/' + 'C{}Trace{:05}.txt'.format(self.channel, self.traceno), dtype=float, skiprows=5, ndmin = 0, delimiter = ',', unpack=False)

    def loadTracePD(self):
        '''Load a trace from txt record into numpy array. Faste than loadTrace'''
        self.trace = pd.read_csv(self.config['data_folder'] +'/' + self.config['analyze_date'] + '/' + 'C{}Trace{:05}.txt'.format(self.channel, self.traceno), skiprows = 5, delimiter = ",", dtype = float, )
        self.trace = self.trace.to_numpy()

    def updateConfig(self):
        config1 = open('config.json', 'r')
        config = json.load(config1)
        config1.close()
        self.config = config

    def lowpassFilter(self, plot = False):
        '''Low pass filter signal. Parameters are tunable in config.'''
        normalized_cutoff = self.config['cutoff'] / (0.5 * self.config['sample_rate']) 
        assert((normalized_cutoff >= 0) & (normalized_cutoff <= 1))# needs to be within (0,1)

        self.b, self.a = butter(self.config['order'], normalized_cutoff, 'low', analog = False)
        self.lpvoltage = filtfilt(self.b, self.a, np.abs(self.trace[:,1].reshape(len(self.trace))))
        if plot == True:
            plt.plot(self.trace[:,:1], self.lpvoltage, color = 'purple')

    def getPeaks(self, plot = False):
        '''Find all peaks in a given trace'''
        self.peaks, _ = find_peaks(self.lpvoltage, height = self.config["height"], distance = self.config["distance"], prominence = self.config["prominence"]) #_ is a dict of parameters, discard em
        if plot:
            for peak in self.peaks:
                plt.plot(self.trace[peak,:1], self.lpvoltage[peak], "x", color = 'orange')

    def getCharges(self):
        '''find the charge for a peak.'''
        self.intc = self.config["intc"]
        self.charges = []
        for peak in self.peaks:
            try:
                self.charges.append(simps(self.trace[peak-self.intc:peak+self.intc,1], self.trace[peak-self.intc:peak+self.intc,0]))
            except IndexError:
                self.charges.append(simps(self.trace[peak-peak:peak+self.intc,1], self.trace[peak-peak:peak+self.intc,0]))
                print("asymmetric charge integration") # for peaks at indices less than intc, we can't integrate symmetrically around them to find the charge.
            pass

    def getWidths(self):
        '''find voltage for a single peak'''
        self.widths = []
        #should consider rewriting to do all peaks at once.
        for peak in self.peaks:
            self.widths.append(peak_widths(self.trace[:,1], [peak], 0.5))

    def plotTrace(self):
        '''Plots the given traceno on a given channel'''
        plt.xlabel("Time, s")
        plt.ylabel("Voltage, V")
        plt.title(f"C{self.channel}Trace{self.traceno:05d}")
        plt.plot(self.trace[:,:1], np.abs(self.trace[:,1]), color = 'green')

    def logTrace(self):
        '''Log single trace to file'''
        logging.info(f"C{self.channel}Trace{self.traceno:05d}:{self.peaks}:{self.charges}")

    def logMulti(self):
        '''Log each channel to a file'''
        with open(f"processed_traces/C{self.channel}.csv", 'a', newline='') as f:
            f.write(f"C{self.channel}Trace{self.traceno:05d}:{self.peaks}:{self.charges} \n")


if __name__ == "__main__":
    configu = load_config()
    logging.basicConfig(filename='batch.log',level=logging.INFO)
    x = Trace(configu, 1,29)
    x.loadTracePD()
    x.plotTrace()
    x.lowpassFilter(True)
    x.getPeaks(True)
    x.getCharges()
    #print(x.peaks, x.charges)
    #x.getWidths()
    #x.logMulti()
    plt.show()