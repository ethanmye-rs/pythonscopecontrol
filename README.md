# README

## Overview

- batch.py is a single threaded approach to processing trace data
- batchmulti.py is a multi-threaded approach to processing trace data. It's the one mostly used.
- classdef.py has main trace class definition and methods.
- config.json sets some scope parameters and hosts all kinds of psuedo global variables
- scopesetup.py contains most of the functions for data acquisitions.
- group.py processes the C1.csv...C8.csv into final counts and prints the final decay percentage.
- tests/ contains some of the testing for major python files.

## Installation

You need python 3+ -- I'm using 3.7.9 on a 64 bit machine. You also need pyvisa, matplotlib, numpy and scipy, and pandas. All relevant version numbers are specified in the requirements.txt. You'll also need the VISA backend -- I would recommend NI-VISA, [downloadable here](https://www.ni.com/en-us/support/downloads/drivers/download.ni-visa.html#346210). I'm using version 20.0. If you need to implement more remote control commands, [this](http://cdn.teledynelecroy.com/files/manuals/maui-remote-control-and-automation-manual.pdf) is an excellent reference, as VISA supports a variety of interfaces (USB, GPIB, Ethernet, etc) and frameworks. I've hardcoded the address of the scope into the program, as I don't expect for it to change in my usage.

## Usage

Data is taken by setting the relevant settings in config.json. You may need to set a few other things on the scope, such as input impedance, time offset, etc, especially if it has been used to run different measurements. Once the high voltage is on, just connect the scope and run main.py and the program will start taking data. The data is not immediately transferred over USB to the controller (TODO!), but is instead stored locally on the oscilloscope. The storage directory is configurable under "utilities" in the scope menu. I highly recommend compressing the files when you transfer the data; a few thousand acquisitions on 8 channels will consume 10s of gigabytes uncompressed. Even simple zip compression can get it down by a factor of 10.

## Detector

The inner diameter of the detector consists of tubes 5-8 and is filled with toluene. The outer detectors hold tubes 1-4 and contains mineral oil. The max voltages the R878 tubes can tolerate is 1500V, although they should be run at less than this. On the Fluke 412B, the polarity is set to positive. I also found it useful to normalize the response of each tube by using the COW (Zener diode voltage subtraction) to adjust the relative signal response of each tube. The scope has a histogram feature that should work nicely for this.

## Trial Data

Waveforms.zip contains a run of gain matched (via the COW) oscilloscope data from Nov 16th, 2020. Yoy need to unzip it (it's ~22GB unzipped) into the project directory to run the analysis. I've included the processed trace files C1.csv...C8.csv, for convenience, but if you want to try different peak finding settings, you will need to rerun the batchmulti.py or batch.py. Depending on what kind of computer you run this on, you may need to adjust the core setting in multiprocessing. It's easy to setup multiprocessing in such a way as introduce errors, so batch.py should produce consistent results.

## Notes about group.py

group.py, as it exists now, likely will not correctly analyze real data. Previous versions (see git) will run and produce a good looking number, but are incorrect.

I originally thought I was finished with group.py. However, upon further reflection, I made some serious errors in previous versions of the code (see gitlab) that necessitate a rewrite. They were not programming issues, but rather fundamental problems in how I classified full and partial decays. Looking at the flowchart, we can see two major pathways for full and partial muon decays.

The first involves a muon entering then decaying in the outer detector. This will appear as two pulses in the outer detector, if the electron stays exclusively in the outer detector. However, it can enter the inner detector, producing another pulse.

The other major pathway involves a muon entering the OD, entering the ID, then decaying inside the ID. If the electron is fully contained, we only get two pulses, but if it escapes, we get a total of three pulses.

Both these events produce three pulses, with similar chronology. This is a big problem, because we're interested in distinguishing between these two events! Thus, we need another way to distinguish between them. We can use the pulse size -- a muon that decays in the ID and whose electron drifts into the OD is expected to be different magnitude than the electron produced by a decay in the OD drifting into the ID.

However, this introduces a "magic number" that can be played with to get an acceptable result. Many of the peak finding parameters (in config.json) are similarly fiddly, but this one is much more direct in effect. Perhaps there is a systematic way to find this, although I'm not sure.

While I think most of the infrastructure (trace acquisition, processing, peakfinding, etc) in this repo is correct, I would recommend redoing the analysis. I did not fully understand inner and outer decays, and I've run out of time to implement a working solution, especially with me being not very good with pandas. Make sure you understand what each decay will look like on the scope, as it's the only way to classify events correctly!

## Further TODOs or areas of expansion

Redo group.py
Implement a logging system for processing data
Use binary data output instead of ASCII for better size
Implement additional scope setup commands
Implement USB transfer
