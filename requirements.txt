###### Requirements without Version Specifiers ######`

###### Requirements with Version Specifiers ######`
numpy ~= 1.18.5
pandas ~= 1.2.0
matplotlib ~= 3.3.1
scipy ~= 1.5.2
pyvisa ~= 1.11.3


