import sys
sys.path.append('../pythonscopecontrol/')

from group import *

#tests for the group python file. run with python -m pytest

def test_parseline():
    #applymap handles the na values in na ignore
    assert parseline('4989   2478 22784  16588  2474   2493  2739  3307 10876    ') == (4989, 2478, 22784, 16588, 2474, 2493, 2739, 3307, 10876)
    assert parseline("4      -17      2479    ") == (4, -17, 2479)

def test_lenline():
    #test no double events
    d = {'Name': ['C1Trace00005'], 'Tube1': [[2467]], 'Tube2': [[np.nan]], 'Tube3': [[np.nan]], 
    'Tube4': [[np.nan]], 'Tube5': [[np.nan]], 'Tube6': [[np.nan]], 'Tube7': [[np.nan]], 'Tube8': [[np.nan]]}
    df = pd.DataFrame(data=d)
    test = df.apply(lenline, args=(1,), axis = 1)
    assert test.iloc[0] == True

    #test double in position 1
    d = {'Name': ['C1Trace00005'], 'Tube1': [[2467,1234]], 'Tube2': [[2467]], 'Tube3': [[5678]], 
    'Tube4': [[2467]], 'Tube5': [[np.nan]], 'Tube6': [[np.nan]], 'Tube7': [[2467]], 'Tube8': [[np.nan]]}
    df = pd.DataFrame(data=d)
    test = df.apply(lenline, args=(3,), axis = 1)
    assert test.iloc[0] == True

    #test double in position 5
    d = {'Name': ['C1Trace00005'], 'Tube1': [[2467]], 'Tube2': [[2467]], 'Tube3': [[5678]], 
    'Tube4': [[2467]], 'Tube5': [[2467,1245]], 'Tube6': [[np.nan]], 'Tube7': [[2467]], 'Tube8': [[np.nan]]}
    df = pd.DataFrame(data=d)
    test = df.apply(lenline, args=(3,), axis = 1)
    assert test.iloc[0] == True

    #test 2 doubles in arb position
    d = {'Name': ['C1Trace00005'], 'Tube1': [[2467]], 'Tube2': [[2467,1234]], 'Tube3': [[5678]], 
    'Tube4': [[2467]], 'Tube5': [[2467,1245]], 'Tube6': [[np.nan]], 'Tube7': [[2467]], 'Tube8': [[np.nan]]}
    df = pd.DataFrame(data=d)
    test = df.apply(lenline, args=(3,), axis = 1)
    assert test.iloc[0] == True

    #test a triple and a double in arb position
    d = {'Name': ['C1Trace00005'], 'Tube1': [[2467]], 'Tube2': [[2467]], 'Tube3': [[5678,9876]], 
    'Tube4': [[2467]], 'Tube5': [[2467,1245,4567]], 'Tube6': [[np.nan]], 'Tube7': [[2467]], 'Tube8': [[np.nan]]}
    df = pd.DataFrame(data=d)
    test = df.apply(lenline, args=(2,), axis = 1)
    assert test.iloc[0] == True

def test_id_decay():
    #non-physical
    d = {
    'Name': ['C1Trace00417'],
    'tube1': [[2471]],
    'tube2': [[2466]],
    'tube3': [[np.nan]],
    'tube4': [[2900]], 
    'tube5': [[np.nan]], 
    'tube6': [[np.nan]], 
    'tube7': [[2800]], 
    'tube8': [[4444]],
    'charge_1': -4.850160012833306e-09,
    'charge_2': -2.3204000463697455e-10,
    'charge_3': np.nan,
    'charge_4': -4.850160012833306e-09,
    'charge_5': np.nan,
    'charge_6': np.nan,
    'charge_7': -4.850160012833306e-09,
    'charge_8': -5.995016002833306e-09,
    }

    df = pd.DataFrame(data=d)
    test = df.apply(id_decay, args=(3,), axis = 1)

if __name__ == "__main__":
    test_id_decay()