import time
import glob
import logging
import fileinput
import csv
import multiprocessing as mp
import matplotlib.pyplot as plt
from classdef import Trace
from scopesetup import load_config

def checktrace(channel, trace):
    config = load_config()
    x = Trace(config, channel, trace)
    x.loadTracePD()
    x.lowpassFilter(False)
    x.getPeaks(False)
    x.getCharges()
    x.logMulti()

if __name__ == "__main__":
    start = time.perf_counter()
    workers = mp.cpu_count()
    print(f"Workers Avail: {workers}")
    pool = mp.Pool(workers)

    for j in range(0,5000):
        for i in range(1, 9):
            pool.apply_async(func = checktrace, args = [i,j])

    pool.close()
    pool.join()
    finish = time.perf_counter()
    print(f"Processing time: {finish - start} seconds")