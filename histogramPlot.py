import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy.stats as ss
import numpy as np
import csv

def meanval(a, b):
    sum = 0
    assert len(a) == len(b)
    for i in range(0,len(a)):
        sum += a[i]*b[i]
    return sum / len(a)

expt = lambda t, a, b: a * np.exp(-b * t)

filename = "normalization_data/6th/M1Trace00000.txt"

with open (filename) as f:
    [next(f) for i in range(0,5)] # skip the first few header lines
    reader = csv.reader(f)

    tl = []
    cl = []

    for row in reader:
        if float(row[0]) >= 1e-15:
            tl.append(float(row[0]))
            cl.append(float(row[1]))

fit = curve_fit(expt, tl, cl)

plt.plot(tl, [expt(t, fit[0][0], fit[0][1]) for t in tl], color = 'red')
plt.title(f"a = {fit[0][0]:.2f}, b = {fit[0][1]:.2f}, Mean Value = {meanval(tl, cl)*100:.2f} mS")
plt.bar(tl,cl, width = 0.005)
plt.ylabel("Count #")
plt.xlabel("mS")
print(f" Mean value: {meanval(tl, cl)*100}mS, a={fit[0][0]}, b={fit[0][1]} ")

plt.show()