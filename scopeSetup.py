import pyvisa as visa
import json
import csv
from time import sleep

# https://pyvisa.readthedocs.io/en/1.8/api/resources.html#pyvisa.resources.GPIBInterface
# http://cdn.teledynelecroy.com/files/manuals/maui-remote-control-and-automation-manual.pdf

def setup_scope():
    rm = visa.ResourceManager()
    #print(rm.list_resourceopens.ps())
    lecroy = rm.open_resource('USB0::0x05FF::0x1023::3805N01981::INSTR')
    print(lecroy.query("*IDN?"))
    
    lecroy.timeout = int(config["timeout"])
    print("Timeout set to {} ms".format(config["timeout"]))

    for i in range(len(config["channel_list"])):
        lecroy.write("C" + str(config["channel_list"][i]) + ":TRA ON")
    print("Trace {} ON".format(config["channel_list"]))

    lecroy.write("TDIV " + config["timebase"])
    print("Timebase set to {}".format(lecroy.query("TIME_DIV?")))

    #lecroy.write(config["trigger_source"] + ":TRLV " + config["trigger_level"])
    #print("Triger set to {}".format(lecroy.query(config["trigger_source"] + ":TRIG_LEVEL?")))

    print("Connection OK!")
    return lecroy

def load_config():
    config1 = open('config.json', 'r')
    config = json.load(config1)
    config1.close()
    return config

def storage_measurement(lecroy):
    lecroy.write("STOP")
    lecroy.write("CHDR OFF")
    sleep(20) #may be needed, INR took on some funny values, >>>1, -- related to self cal?
    print(lecroy.query("INR?")) #this throws back >>1 occasionally
    lecroy.write("TRMD SINGLE")
    for i in range(int(config["acquisitions"])):
        while int(lecroy.query("INR?")) == 0:
            sleep(0.05) 
        lecroy.write("STO ALL_DISPLAYED,FILE")
        lecroy.write("TRMD SINGLE")
    print("Done!")

config = load_config()
