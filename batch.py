import matplotlib.pyplot as plt
import glob
from classdef import Trace
from scopesetup import load_config
import logging
import time

#a single threaded approach to processing waveform data

if __name__ == "__main__":
    start = time.perf_counter()
    config = load_config()
    logging.basicConfig(filename='batch.log',level=logging.INFO)

    for trace in range(6,5000):
        for channel in config["channel_list"]:
            x = Trace(config, channel, trace)
            x.loadTracePD()
            x.lowpassFilter(False)
            x.getPeaks(False)
            x.getCharges()
            x.logMulti()
            print(repr(x))