import scopesetup as setup
import os, os.path

if __name__ == "__main__":
    config = setup.load_config()
    lecroy = setup.setup_scope()
    setup.storage_measurement(lecroy)